/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modifygraphoutput;

import java.io.*;
import java.util.*;
import java.lang.*;


/**
 *
 * @author kanishk
 */
public class ModifyGraphOutput {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try{
            System.out.println(System.getProperty("user.dir"));
            PrintWriter out= new PrintWriter(new FileWriter("hg-annoHeaderCorrection.csv"));
            File newFile=new File("hg-anno.dat");
            FileReader fileReader=new FileReader(newFile);
            BufferedReader reader=new BufferedReader(fileReader);
            String line = null;
            int count=0;
            boolean flag=false;
            while ((line = reader.readLine()) != null) {
                count++;
                if(count==1)
                    line="geneName"+line;
                
                StringTokenizer elements=new StringTokenizer(line);
                StringBuilder outline=new StringBuilder();
                while(elements.hasMoreTokens())
                    outline.append(elements.nextToken()+",");
                out.println(outline.substring(0,outline.length()));
                
            }     
            
        }
        catch(Exception E){
            E.printStackTrace();
        }
    }
    
}
